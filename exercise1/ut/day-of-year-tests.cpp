#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};
/* 
TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(false);
}
*/

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, ReturnMay23rd)
{
  ASSERT_EQ(dayOfYear(5, 23, 2021), 143);
}

TEST(DayOfYearTestSuite, ReturnNovember4th)
{
  ASSERT_EQ(dayOfYear(11, 4, 2021), 308);
}

TEST(DayOfYearTestSuite, LeapYearMay23rd)
{
  ASSERT_EQ(dayOfYear(5, 23, 2020), 144);
}

TEST(DayOfYearTestSuite, LeapYearNovember4th)
{
  ASSERT_EQ(dayOfYear(11, 4, 2020), 309);
}