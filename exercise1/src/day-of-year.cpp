#include "day-of-year.hpp"


int dayOfYear(int month, int dayOfMonth, int year) {
    
    switch(month) {
            case 2:
                dayOfMonth += 31;
                break;
            case 3:
                dayOfMonth += 59;
                break;
            case 4:
                dayOfMonth += 90;
                break;
            case 5:
                dayOfMonth += 120;
                break;
            case 6:
                dayOfMonth += 151;
                break;
            case 7:
                dayOfMonth += 181;
                break;
            case 8:
                dayOfMonth += 212;
                break;
            case 9:
                dayOfMonth += 243;
                break;
            case 10:
                dayOfMonth += 273;
                break;
            case 11:
                dayOfMonth += 304;
                break;
            case 12:
                dayOfMonth += 334;
                break;
        }
    
    if(((year%4 ==0 && year%100 != 0) || year%400 == 0) && month > 2)
        ++dayOfMonth;
        
    return dayOfMonth;
}

